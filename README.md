## :link: Nextcloud Bookmarks Android App

[<img src="assets/nx/icon.png" width=160px>](/)
[![F-Droid](./assets/fdroid_badge.png)](https://f-droid.org/packages/org.schabi.nxbookmarks/)
[![PlayStore](./assets/ps_badge.png)](https://play.google.com/store/apps/details?id=org.bisw.nxbookmarks)
[![Gitter](https://badges.gitter.im/nextcloud-bookmarks/community.svg)](https://gitter.im/nextcloud-bookmarks/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)


An Android front end for the Nextcloud [Bookmark App](https://github.com/nextcloud/bookmarks/) 
based on the new [REST API](https://github.com/nextcloud/bookmarks/#rest-api) that was introduced
by NextCloudBookmarks version [0.10.1](https://github.com/nextcloud/bookmarks/releases/tag/v0.10.1)
and ownCloudBookmarks version [0.10.2](https://marketplace.owncloud.com/apps/bookmarks)

[<img src="assets/nx/screenshots/shot1.png" width=160px>](assets/nx/screenshots/shot1.png)
[<img src="assets/nx/screenshots/shot2.png" width=160px>](assets/nx/screenshots/shot2.png)
[<img src="assets/nx/screenshots/shot3.png" width=160px>](assets/nx/screenshots/shot3.png)
[<img src="assets/nx/screenshots/shot4.png" width=160px>](assets/nx/screenshots/shot4.png)
[<img src="assets/nx/screenshots/shot5.png" width=160px>](assets/nx/screenshots/shot5.png)

## :link: Issues
* Please note we have identified Some issues. Please look at [Issue board](https://gitlab.com/bisada/OCBookmarks/issues) before review.
* Feel free to send us a pull request.
## :link: Maintainer
* [Biswajit Das](https://gitlab.com/bisasda):@bisasda

## :link: Contributors
* [Biswajit Das](https://gitlab.com/bisasda):@bisasda
* [Christian Schabesberger](https://gitlab.com/derSchabi):@derSchabi

## :link: Requirements
* [Nextcloud](https://nextcloud.com/) instance running.

## :link: Contributions
* All pull requests are welcome.